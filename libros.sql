﻿/*creado de la BBDD*/
  CREATE DATABASE IF NOT EXISTS libros;
  USE libros;

/*creado de tablas*/
  CREATE TABLE libro(
    clavelibro int AUTO_INCREMENT PRIMARY KEY,
    titulo varchar(50),
    idioma char(3),
    formato varchar(15),
    categoria varchar(20),
    claveeditorial int
  );

  CREATE TABLE tema(
    clavetema smallint,
    nombre varchar(50)
  );

  CREATE TABLE autor(
    claveautor smallint,
    nombre varchar(50)
  );

  CREATE TABLE editorial(
    claveeditorial smallint AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    direccion varchar(80),
    telefono char(9)
  );

  CREATE TABLE ejemplar(
    clavelibro int,
    numeroordern smallint,
    edicion varchar(20),
    ubicacion varchar(30)
   );

  CREATE TABLE socio(
    clavesocio smallint AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    direccion varchar(80),
    telefono char(10),
    categoria char(20)
  );

  CREATE TABLE trata_sobre(

  );