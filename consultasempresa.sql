﻿/*Consultas BBDD emopresa*/
USE empresa;

/*1.*/
  SELECT * 
    FROM emple;

/*2.*/
  SELECT * 
    FROM depart;
/*3.*/
  SELECT apellido,oficio
    FROM emple;
/*4.*/
  SELECT dept_no,loc
    FROM depart;
/*5.*/
  SELECT * 
    FROM depart;
/*6.*/
  SELECT COUNT(*) AS 'numero de empleados'
    FROM emple;
/*7.*/
  SELECT apellido 
    FROM emple 
    ORDER BY apellido;
/*8.*/
  SELECT apellido 
    FROM emple 
    ORDER BY apellido DESC;
/*9.*/
  SELECT COUNT(*) AS 'numero de departamentos'
    FROM depart d;
/*10.*/
  SELECT  COUNT(*) AS 'total empresa'
    FROM depart d,emple e;
/*11.*/
  SELECT *
    FROM emple e
    ORDER BY e.dept_no DESC;
 /*12.*/
   SELECT *
    FROM emple e
    ORDER BY e.dept_no DESC,e.oficio ASC;
/*13.*/
   SELECT *
    FROM emple e
    ORDER BY e.dept_no DESC, e.apellido ASC;
  /*14.*/
    SELECT emp_no
      FROM emple e
      WHERE e.salario > 2000;
  /*15*/
    SELECT emp_no,e.apellido
      FROM emple e
      WHERE e.salario < 2000;
  /*16*/
    SELECT * 
      FROM emple e 
      WHERE e.salario BETWEEN 1500 AND 2000;
 /*17*/
   SELECT *
    FROM emple e 
    WHERE e.oficio='analista';
 /*18.*/
  SELECT *
    FROM emple e 
    WHERE e.oficio='analista' AND e.salario > 2000;
  /*19.*/
    SELECT e.apellido, e.oficio
      FROM emple e
      WHERE e.dept_no=20;
/*20.*/
  SELECT COUNT(*) AS 'numero de empleados'
    FROM emple e
    WHERE e.oficio='vendedor';
/*21.*/
  SELECT *
    FROM emple e
    WHERE e.apellido LIKE 'm%' OR e.apellido LIKE 'n%'
    ORDER BY e.apellido;

/*22*/
  SELECT e.emp_no,
         e.apellido,
         e.oficio,
         e.dir,
         e.fecha_alt,
         e.salario,
         e.comision,
         e.dept_no
    FROM emple e
    WHERE e.oficio='vendedor'
    ORDER BY e.apellido;
  /*23*/
    SELECT apellido
      FROM emple e
       ORDER BY e.salario DESC
       LIMIT 1;
/*24*/
  SELECT * 
    FROM emple e
    WHERE e.dept_no=10 AND e.oficio='analista'
    ORDER BY e.apellido,e.oficio;
/*25*/
  SELECT MONTH(e.fecha_alt)
    FROM emple e;

/*26.*/
  SELECT YEAR(fecha_alt)
    FROM emple e;

/*27.*/
  SELECT DAY(fecha_alt)
    FROM emple e;
/*28.*/
  SELECT apellido
    FROM emple e
    WHERE e.salario>2000 OR e.dept_no=20;

/*29.*/
  SELECT apellido,dnombre
    FROM depart d
    INNER JOIN emple e ON d.dept_no = e.dept_no;
/*30.*/
  SELECT apellido,oficio,dnombre
    FROM depart d
    INNER JOIN emple e ON d.dept_no = e.dept_no
    ORDER BY e.apellido; 
/*31.*/
  SELECT e.dept_no,COUNT(*) AS 'numero de empleados'
    FROM emple e 
    GROUP BY dept_no;
/*32.*/
  SELECT dnombre,COUNT(*) AS 'numero de empleados'
    FROM depart d
    INNER JOIN emple e ON d.dept_no = e.dept_no
    GROUP BY d.dnombre;
/*33.*/
  SELECT apellido
    FROM emple e
    INNER JOIN depart d ON e.dept_no = d.dept_no
    ORDER BY e.oficio, d.dnombre;
/*34.*/
  SELECT apellido
    FROM emple e
    WHERE e.apellido LIKE 'a%';
/*35.*/
SELECT apellido
  FROM emple e
  WHERE e.apellido LIKE 'a%' or'%m';
/*36.*/
  SELECT *
    FROM emple e 
    WHERE e.apellido NOT LIKE '%z';
/*37.*/
  SELECT *
    FROM emple e
    WHERE e.apellido LIKE 'a%' AND e.oficio LIKE '%e%'
    ORDER BY e.oficio DESC,e.oficio DESC;