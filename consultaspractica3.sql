﻿/*consultas practica3*/
USE practica3;
/*1.*/
  SELECT e.dept_no,COUNT(*) AS 'numero de empleados'
    FROM emple e
    GROUP BY e.dept_no;
/*2.*/
  SELECT e.dept_no,COUNT(*) AS 'numero de empleados'
    FROM emple e
    GROUP BY e.dept_no
    HAVING `numero de empleados`>5;
/*3.*/
  SELECT e.dept_no,AVG(e.salario) AS 'salario medio'
    FROM emple e
    GROUP BY e.dept_no;
/*4.*/
  SELECT e.apellido,d.dnombre
    FROM emple e
    INNER JOIN depart d ON e.dept_no = d.dept_no
    WHERE d.dnombre='ventas' AND e.oficio='vendedor';
/*5.*/
  SELECT COUNT(*) AS 'numero de vendedores dpto ventas'
    FROM emple e
     INNER JOIN depart d ON e.dept_no = d.dept_no
     WHERE d.dnombre='ventas' AND e.oficio='vendedor';
/*6.*/
  SELECT e.oficio 
    FROM emple e
    INNER JOIN depart d ON e.dept_no = d.dept_no
    WHERE d.dnombre='ventas';
/*7.*/
  SELECT e.dept_no,COUNT(*) AS 'numero de empleados'
    FROM emple e
    WHERE e.oficio = 'empleado'
    GROUP BY e.dept_no;

/*8.*/
  SELECT e.dept_no,COUNT(*) AS 'numero e empleados'
    FROM emple e
    GROUP BY e.dept_no DESC
    LIMIT 1;

/*9.*/
  SELECT e.dept_no,sum(salario) AS 'suma de salarios'
    FROM emple e
    GROUP BY e.dept_no
    HAVING 'suma de salarios' > (SELECT AVG(e.salario) FROM emple e);

/*10.*/
  SELECT e.oficio,SUM(salario)
    FROM emple e
    GROUP BY e.oficio;

/*11*/
  SELECT e.oficio,SUM(salario)
    FROM emple e
    INNER JOIN depart d ON e.dept_no = d.dept_no
    WHERE d.dnombre='ventas'
    GROUP BY e.oficio;

/*12*/
  SELECT e.dept_no,COUNT(*) AS 'numero de trabajadores'
    FROM emple e
    WHERE e.oficio='empleado'
    GROUP BY e.dept_no
    ORDER BY `numero de trabajadores` DESC
    LIMIT 1;

/*13*/
  SELECT DISTINCT e.oficio
    FROM emple e
    GROUP BY e.dept_no;

/*14*/
   SELECT e.dept_no, COUNT(*)
    FROM emple e
    GROUP BY e.dept_no
    HAVING COUNT(oficio)>2;

/*15*/
  SELECT h.estanteria,SUM(h.unidades) AS 'numero de unidades'
    FROM herramientas h 
    GROUP BY h.estanteria;

/*16*/
  SELECT h.estanteria,COUNT(*) 
    FROM herramientas h
    GROUP BY h.estanteria
    ORDER BY h.unidades 
    LIMIT 1;

/*17*/
  SELECT m.cod_hospital,COUNT(*) AS 'numero de medicos'
    FROM medicos m
    GROUP BY m.cod_hospital;

/*18*/
  SELECT m.cod_hospital,m.especialidad
    FROM medicos m;

/*19*/
  SELECT m.cod_hospital,m.especialidad,COUNT(*) AS 'numero de medicos' 
    FROM medicos m
    GROUP BY m.cod_hospital,m.especialidad;
/*20*/
  SELECT m.cod_hospital,COUNT(*) AS 'numero de empleados'
    FROM medicos m
    GROUP BY m.cod_hospital;
/*21*/
  SELECT m.especialidad,COUNT(*) AS 'numero de trabajadores'
    FROM medicos m
    GROUP BY m.especialidad;
/*22*/
  SELECT m.especialidad,COUNT(*) AS 'numero de medicos'
    FROM medicos m
    GROUP BY m.especialidad 
    LIMIT 1;
/*23*/
  SELECT h.nombre,MAX(h.num_plazas) AS 'numero de plazas'
    FROM hospitales h;
/*24*/
  SELECT h.estanteria
    FROM herramientas h
    ORDER BY h.estanteria DESC;
/*25*/
  SELECT h.estanteria,h.unidades
    FROM herramientas h
    GROUP BY h.estanteria;


     
   
  
    


     

