﻿CREATE DATABASE IF NOT EXISTS VideoClubCantabria;
USE VideoClubCantabria;

CREATE TABLE peliculas(
   codigopelicula char(5) PRIMARY KEY,
   titulo char(40) NOT NULL,
   genero char(15),
   año int,
   pais char(15)
);

CREATE TABLE socios(
  numerosocio char(5) PRIMARY KEY,
  DNI char(10) UNIQUE NOT NULL,
  nombre char(20),
  apellidos char(40),
  direccion char(50),
  telefono char(10),
  fechanacimiento date
);

CREATE TABLE prestamos(
  codigoprestamo int,
  numerosocio char(5),
  codigopelicula char(5),
  CONSTRAINT PRIMARY KEY(codigoprestamo),
  FOREIGN KEY (numerosocio) REFERENCES socios (numerosocio),
  FOREIGN KEY (codigopelicula) REFERENCES peliculas (codigopelicula)
);

/*insercion de datos*/
  INSERT INTO peliculas VALUES
          ('P001','Rebelión en la Ondas','Comedia','1987',''),
          ('P002','Rocky','Drama','1976',''),
          ('P003','El exorcista','Terror','1973',''),
          ('P004','Ronin','policiaca','1998',''),
          ('P005','Rocky II','drama','1979',''),
          ('P006','Cyrano de bargeraç','drama','1990',''),
          ('P007','Rocky III','drama','1982',''),
          ('P008','EL Resplandor','terror','1980',''),
          ('P009','Sin Perdón','Western','1992','');

 INSERT INTO socios VALUES
      ('S001','10101010A','Fernando','Alonso','Calle Rueda','985332211','1980/02/06/'),
      ('S002','20202020B','Elmer','Bennet','Avda Castilla','915852266','1985/5/12'),
      ('S003','30303030C','Sofia','Loren','Plaza Mayor','942335544','1903/07/08'),
      ('S004','40404040D','Mar','Flores','Calle Alegria','914587895','1978/05/31'),
      ('S005','50505050E','Tamara','Torres','Calle Perejil','935754875','1975/03/24'),
      ('S006','11111111F','Federico','Trillo','Calle Honduras','942225544','1950/04/29'),
      ('S007','11223344G','Marina','Castaño','Avda Jeta','916589878','1957/01/15'),
      ('S008','72727272H','Diego','Tristán','Calle Galicia','942353535','1979/10/19'),
      ('S009','25252525J','Andrei','Shevchenko','Calle Milán','915544545','1982/03/26');

INSERT INTO prestamos VALUES
    ('001','S001','P001'),
    ('002','S002','P003'),
    ('003','S003','P004'),
    ('004','S005','P006'),
    ('005','S007','P001'),
    ('006','S008','P002'),
    ('007','S008','P003'),
    ('008','S009','P001'),
    ('009','S004','P005'),
    ('010','S006','P007'),
    ('011','S001','P008'),
    ('012','S002','P009');







