﻿CREATE DATABASE IF NOT exists pruebas;
USE pruebas;

CREATE TABLE editorial(
  nombre_e char(20) PRIMARY KEY,
  direccion char(20) NOT NULL,
  ciudad char(15),
  pais char(15)
  );

CREATE TABLE libro(
  codigo char(3),
  titulo char(50) UNIQUE,
  idioma char(25),
  nombre_e char(20),
  PRIMARY KEY (codigo),
  FOREIGN KEY (nombre_e) REFERENCES editorial(nombre_e)
  ON DELETE set NULL
  on UPDATE CASCADE
);

INSERT INTO editorial VALUES ('Cantabria','Calle Alonso','Santander','España'),
  ('Santillana','Calle Burgos','Santander','España'),
  ('SM','Calle Cadiz','Santander','España'),
  ('Anaya','Calle Se','Santander','España');

INSERT INTO libro VALUE ('pil','los pilares de la tierra','Español','SM');
INSERT INTO libro VALUE ('idi','idiot','Español','Anaya');

DELETE FROM editorial WHERE nombre_e='SM';

