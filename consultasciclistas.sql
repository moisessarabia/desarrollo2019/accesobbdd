﻿/*consultas BBDD ciclistas */
/*Consultas de seleccion*/
/*0*/
USE ciclistas;
/*1*/
SELECT DISTINCT edad
  FROM ciclista c;
/*2*/
SELECT edad
  FROM ciclista c
  WHERE c.nomequipo='artiach';
/*3*/
SELECT edad,c.nomequipo
  FROM ciclista c
  WHERE c.nomequipo='artiach' OR c.nomequipo='amore vita';
/*4*/
SELECT c.dorsal,c.edad 
  FROM ciclista c
  WHERE c.edad<25 OR c.edad>30;
/*5*/
SELECT c.dorsal, c.edad 
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32 
    AND c.nomequipo='Banesto';
/*6*/
SELECT c.nombre 
  FROM ciclista c 
  WHERE  CHAR_LENGTH(nombre)>8;
/*7*/
SELECT c.dorsal, UPPER(c.nombre) AS 'nombre mayusculas'
  FROM ciclista c;
/*8*/
 SELECT  DISTINCT l.dorsal 
  FROM lleva l
  WHERE l.código='MGE';
/*9*/
SELECT p.nompuerto 
  FROM puerto p
  WHERE p.altura>1500;
/*10*/
SELECT p.dorsal 
  FROM puerto p
  WHERE p.pendiente>8 OR p.altura BETWEEN 1800 AND 3000;
/*11*/
SELECT p.dorsal 
  FROM puerto p
  WHERE p.pendiente>8 AND p.altura BETWEEN 1800 AND 3000;
/*consultas de totales*/
/*1*/
SELECT COUNT(*) AS 'numero de ciclistas'
  FROM ciclista c;
/*2*/
SELECT COUNT(*) AS 'numero de ciclistas del Banesto'
  FROM ciclista c
  WHERE c.nomequipo='banesto';
/*3*/
SELECT AVG(edad) AS 'media de edad ciclistas'
  FROM ciclista c;
/*4*/
  SELECT AVG(edad) AS 'media de edad ciclistas del Banesto'
  FROM ciclista c
  WHERE c.nomequipo='banesto';
/*5*/
  SELECT c.nomequipo,AVG(edad) AS 'media de edad'
    FROM ciclista c
    GROUP BY c.nomequipo;
/*6*/
SELECT c.nomequipo,COUNT(*) AS 'numero de ciclistas'
  FROM ciclista c
  GROUP BY c.nomequipo;
/*7*/
SELECT COUNT(*) AS 'numero de puertos'
  FROM puerto p;
/*8*/
SELECT COUNT(*) AS 'numero de puertos >1500m'
  FROM puerto p
  WHERE p.altura>1500;
/*9*/
SELECT c.nomequipo
  FROM ciclista c 
  GROUP BY c.nomequipo
  HAVING COUNT(*)>4;
/*10*/
  SELECT c.nomequipo
  FROM ciclista c 
  WHERE c.edad BETWEEN 28 AND 32
  GROUP BY c.nomequipo
  HAVING COUNT(*)>4;
/*11*/
  SELECT dorsal,COUNT(*) AS 'numero de etapas'
    FROM etapa e
    GROUP BY e.dorsal;
/*12*/
  SELECT e.dorsal 
    FROM etapa e
    GROUP BY e.dorsal
    HAVING COUNT(*)>1;
   