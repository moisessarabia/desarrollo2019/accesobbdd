﻿/*creacion de la BBDD*/
CREATE DATABASE IF NOT EXISTS meteorologia;
USE meteorologia;

/*creacion de tablas*/
CREATE TABLE estacion(
  identificador mediumint UNSIGNED PRIMARY KEY,
  latitud varchar(14),
  longitud varchar(15),
  altitud mediumint(15)
);

CREATE TABLE IF NOT EXISTS muestra(
  identificadorestacion mediumint UNSIGNED,
  fecha date,
  temperaturaminima tinyint,
  temperaturamaxima tinyint,
  precipitaciones smallint UNSIGNED,
  humedadminima tinyint UNSIGNED,
  humedadmaxima tinyint UNSIGNED,
  velvientomin smallint UNSIGNED,
  velvientomax smallint UNSIGNED,
  PRIMARY KEY(identificadorestacion,fecha)
);

ALTER TABLE muestra 
  ADD CONSTRAINT idestacion FOREIGN KEY (identificadorestacion) 
  REFERENCES estacion(identificador) ON DELETE CASCADE;
  
