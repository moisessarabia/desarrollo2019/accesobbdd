﻿CREATE DATABASE IF NOT exists noticias;
USE noticias;

CREATE OR REPLACE TABLE noticias(
  id int AUTO_INCREMENT NOT NULL PRIMARY KEY,
  titulo varchar(100),
  texto text 
);

INSERT INTO noticias VALUES 
(1, 'se ha incendiado un avion', 'jbcihewdciwdnc dsk jjwcgywdgcwhdsb dwchucvgsdiajñ dwjcqi'),
(2,'se ha estrellado un coche','ciwhyf9ewchijsdb sbvauydscguhasdc vahscguasxvcuwdoqft7whpiuzcghis'),
(3, 'se ha derrumbado un edificio','djfiweiuwdifdge8qwy63276dusqduyqweduyqascxhqwdif9iwuedfdg');