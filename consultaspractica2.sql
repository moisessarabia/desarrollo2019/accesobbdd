﻿USE trabajadores;
/*1.*/
  SELECT COUNT(*) AS 'numero de ciudades'
    FROM ciudad c;
/*2.*/
  SELECT nombre 
    FROM ciudad
    WHERE población > (SELECT AVG(población) AS media
                        FROM ciudad c);
  /*3*/
   SELECT nombre 
    FROM ciudad
    WHERE población < (SELECT AVG(población) AS media
                        FROM ciudad c);
  /*4*/
    SELECT nombre 
      FROM ciudad c
      ORDER BY c.población DESC
      LIMIT 1;
/*5*/
   SELECT nombre 
      FROM ciudad c
      ORDER BY c.población ASC
      LIMIT 1; 
/*6*/
   SELECT COUNT(*)
    FROM ciudad
    WHERE población > (SELECT AVG(población) AS media
                        FROM ciudad c);
/*7*/
  SELECT p.ciudad,COUNT(*) AS 'numero de personas'
    FROM persona p
    GROUP BY ciudad;
/*8*/
  SELECT t.compañia,COUNT(*) AS 'numero de personas'
    FROM trabaja t
    GROUP BY compañia;
/*9*/
  SELECT t.compañia,COUNT(*) AS 'numero de personas'
    FROM trabaja t
    GROUP BY compañia
    ORDER BY `numero de personas` DESC
    LIMIT 1;
/*10*/
  SELECT compañia,AVG(salario) AS 'salario medio'
    FROM trabaja
    GROUP BY compañia;
/*11*/


/*15*/
  SELECT persona,s.supervisor
    FROM supervisa s;
/*17*/
  SELECT DISTINCT COUNT(*)
    FROM compañia c;

/*18*/
SELECT DISTINCT COUNT(*)
    FROM persona p;

/*19*/
  SELECT t.persona 
    FROM trabaja t
    WHERE t.compañia='FAGOR';

/*20*/
   SELECT t.persona 
    FROM trabaja t
    WHERE t.compañia !='FAGOR';

/*21*/
  SELECT COUNT(*)
    FROM trabaja t
    WHERE t.compañia ='INDRA';

/*22*/
  SELECT t.persona 
    FROM trabaja t
    WHERE t.compañia='FAGOR' OR t.compañia='INDRA';
