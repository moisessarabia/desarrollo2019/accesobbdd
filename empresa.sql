﻿/*creado de la BBDD*/
CREATE DATABASE IF NOT EXISTS empresa;

USE empresa;

/*creado de las tablas*/
CREATE TABLE depart(
    dept_no int PRIMARY KEY,
    dnombre varchar(30),
    loc varchar(30)
);

CREATE TABLE emple(
  emp_no int,
  apellido varchar(50) NOT NULL,
  oficio varchar(30),
  dir int,
  fecha_alt date,
  salario int,
  comision int,
  dept_no int,
  PRIMARY KEY (emp_no),
  CONSTRAINT numdpto FOREIGN KEY(dept_no)REFERENCES depart(dept_no)
  ON DELETE CASCADE
  on UPDATE CASCADE
);

/*Insercion de datos*/
INSERT INTO depart VALUES 
         (10, 'contabilidad','sevilla'),
         (20,'investigacion','madrid'),
         (30,'ventas','barcelona'),
         (40,'produccion','bilbao');

INSERT INTO emple VALUES 
         (7369,'sanchez','empleado',7902,'1990/12/17',1040,'',20),
         (7499,'arroyo','vendedor',7698,'1990/02/20',1500,390,30),
         (7521,'sala','vendedor',7698,'1991/02/22',1625,650,30),
         (7566,'jimenez','director',7839,'1991/04/02',2900,'',20),
         (7654,'martin','vendedor',7698,'1991/09/29',1600,1020,30),
         (7698,'negro','director',7839,'1991/05/01',3005,'',30),
         (7782,'cerezo','director',7839,'1991/06/09',2885,'',10),
         (7788,'gil','analista',7566,'1991/11/09',3000,'',20),
         (7839,'rey','presidente',NULL,'1991/11/17',4100,'',10),
         (7844,'tovar','vendedor',7698,'1991/09/08',1350,0,30),
         (7876,'alonso','empleado',7788,'1991/23/09',1430,'',20),
         (7900,'jimeno','empleado',7698,'1991/12/03',1335,'',30),
         (7902,'fernandez','analista',7566,'1991/12/03',3000,'',20),
         (7934,'muñoz','empleado',7782,'1992/01/23',1690,'',10);
         

